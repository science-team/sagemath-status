#! /usr/bin/python3
#
# dss.py -- generation script for the GNOME in Debian status page
#           modified to show the Sage status page instead
# Copyright (C) 2008-2011 Frederic Peters
# Copyright (C) 2012      Tobias Hansen
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from functools import cmp_to_key
import hashlib
import urllib3
from urllib.parse import quote
import re
import os
import sys
import apt_pkg
import subprocess
import time
import socket
import pickle
import json
from optparse import OptionParser
from io import StringIO

TESTING = 'testing'
UNSTABLE = 'unstable'

try:
    import ZSI.client
except ImportError:
    ZSI = None

try:
    import SOAPpy
except ImportError:
    SOAPpy = None

apt_pkg.init()

http = urllib3.PoolManager(cert_reqs='REQUIRED', ca_certs='cacert.pem')

CACHE_DIR = 'debian-sage-status-cache'

HTML_AT_TOP = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Status of Sage %(version)s in Debian</title>
<script type="text/javascript" src="debian-gnome.js"></script>
<style type="text/css">
html, body {
    font-family: sans-serif;
    margin: 0;
    padding: 0;
    background: #eee;
}
#logo {
    position: absolute;
    top: 10px;
    left: 10px;
    border: 0px;
    z-index: 10;
}
#logo2 {
    position: absolute;
    top: 10px;
    left: 84px;
    border: 0px;
    z-index: 10;
}

#header h1 {
    margin: 0;
    padding: 5px 0px 5px 158px;
    background: #555753;
    color: white;
    margin-bottom: 1em;
    border-bottom: 2px solid #aaa;
}

div#content {
    margin: 2em;
    margin-right: 15em;
}

tfoot th, thead th {
    font-weight: normal;
}
tr.notneeded {
    display: none;
}
td.newer-testing, span.newer-testing { background: rgba(127, 127, 255, 0.4); }
td.newer, span.newer { background: rgba(127, 127, 255, 0.7); }
td.ok-testing, span.ok-testing, td.uptodate3 { }
td.ok, span.ok { background: rgba(153, 255, 153, 0.5); }
td.ok-experimental, span.ok-experimental, td.uptodate2 { background: #fcf9af; }
td.waiting-in-new, span.waiting-in-new { background: #fe4; }
td.waiting-in-new td:last-child { display: none; }
td.lagging-minor, span.lagging-minor, td.uptodate1 { background: #fcaf3e; }
td.lagging-major, span.lagging-major { background: #ef2929; }
td.missing, span.missing, td.uptodate0 { background: #a00; color: #fff; }
td.notneeded, span.notneeded { text-decoration: line-through; }

td.heading {
    text-align: center;
    background: #555753;
    color: white;
    font-weight: bold;
}
tbody th, tbody td {
    background: #d3d7cf;
}
tbody th, tbody td.pts {
    text-align: left;
}
tbody td {
    text-align: center;
}
tfoot td {
    padding-top: 1em;
    vertical-align: top;
}
td.pts a {
    font-size: small;
    padding: 0 1ex;
}
p#footer {
    font-size: small;
    color: gray;
    margin: 0;
}
div#control {
    text-align: right;
    position: fixed;
    right: 2em;
    font-size: small;
    width: 15em;
}
ul#legend {
    list-style: none;
}
ul#legend li {
    margin: 0.5ex 0;
    text-align: center;
}
ul#legend li span {
    display: block;
}
span#checkboxes {
    display: inline-block;
    text-align: left;
}
tr.arch-details {
    display: none;
}
tr.arch-details td {
    font-size: small;
    text-align: right;
}
p#summary {
    position: absolute;
    right: 1em;
    top: -1ex;
    color: white;
    text-align: right;
}
</style>
</head>
<body>
 <div id="header">
  <img id="logo" src="sage64.png" alt="SAGE">
  <img id="logo2" src="debian64.png" alt="Debian">
  <h1>Status of Sage %(version)s in Debian</h1>
 </div>
 <div id="content">

<div id="control">
<ul id="legend">
 <li><span class="newer-testing">Higher version in %(testing)s</span></li>
 <li><span class="newer">Higher version in %(unstable)s</span></li>
 <li><span class="ok-testing">Up-to-date in %(testing)s</span></li>
 <li><span class="ok">Up-to-date in %(unstable)s</span></li>
 <li><span class="ok-experimental">Up-to-date in experimental</span></li>
 <li><span class="waiting-in-new">Waiting in NEW</span></li>
 <li><span class="lagging-minor">Not up-to-date, lagging by a minor version</span></li>
 <li><span class="lagging-major">Not up-to-date, lagging by a major version</span></li>
 <li><span class="missing">Not in Debian</span></li>
 <li><span class="notneeded">Not needed</span></li>
</ul>

<span id="checkboxes">
<input type="checkbox" id="unneeded" onclick="display_unneeded()"><label for="unneeded">Display unneeded</label>
<br>
<input type="checkbox" checked="checked" id="ok-testing" onclick="display_ok_testing()"><label for="ok-testing">Display OK-in-%(testing)s</label>
<br>
<input type="checkbox" checked="checked" id="newer-testing" onclick="display_newer_testing()"><label for="newer-testing">Display newer-in-%(testing)s</label>
<br>
<input type="checkbox" checked="checked" id="ok-unstable" onclick="display_ok_unstable()"><label for="ok-unstable">Display OK-in-%(unstable)s</label>
<br>
<input type="checkbox" checked="checked" id="newer" onclick="display_newer()"><label for="newer">Display newer</label>
</span>
</div>


<table>
<thead>
<tr><td></td> <th>Sage</th> <th colspan="2">Debian</th> </tr>
</thead>
<tbody>

'''

DEBIAN_NAME_MAPPING = {
    'arb': 'flint-arb',
    'argcomplete': 'python-argcomplete',
    'argon2_cffi': 'python-argon2',
    'asttokens': 'python-asttokens',
    'attrs': 'python-attrs',
    'babel': 'python-babel',
    'backcall': 'python-backcall',
    'cffi': 'python-cffi',
    'gc': 'libgc',
    'beniget': 'python-beniget',
    'bleach': 'python-bleach',
    'boost_cropped': 'boost-defaults',
    'charset_normalizer': 'python-charset-normalizer',
    'combinatorial_designs': 'sagemath-database-combinatorial-designs',
    'conway_polynomials': 'sagemath-database-conway-polynomials',
    'cycler': 'python-cycler',
    'cypari': 'cypari2',
    'dateutil': 'python-dateutil',
    'decorator': 'python-decorator',
    'deprecation': 'python-deprecation',
    'docutils': 'python-docutils',
    'ecm': 'gmp-ecm',
    'editables': 'python-editables',
    'elliptic_curves': 'sagemath-database-elliptic-curves',
    'executing': 'python-executing',
    'fastjsonschema': 'python-fastjsonschema',
    'fflas_ffpack': 'fflas-ffpack',
    'filelock': 'python-filelock',
    'flask_autoindex': 'flask-autoindex',
    'flask_babel': 'flask-babel',
    'flask_oldsessions': 'flask-oldsessions',
    'flask_openid': 'flask-openid',
    'flask_silk': 'flask-silk',
    'flit_core': 'flit',
    'future': 'python-future',
    'gast': 'python-gast',
    'gcc': 'gcc-9',
    'gd': 'libgd2',
    'gdmodule': 'python-gd',
    'graphs': 'sagemath-database-graphs',
    'hatch_vcs': 'hatch-vcs',
    'idna': 'python-idna',
    'imagesize': 'python-imagesize',
    'importlib_metadata': 'python-importlib-metadata',
    'importlib_resources': 'importlib-resources',
    'ipaddress' : 'python-ipaddress',
    'ipython_genutils' : 'ipython-genutils',
    'jedi': 'python-jedi',
    'itsdangerous': 'python-itsdangerous',
    'jupyter_client': 'jupyter-client',
    'jupyter_core': 'jupyter-core',
    'jupyter_packaging': 'jupyter-packaging',
    'jupyter_sphinx': 'jupyter-sphinx',
    'jupyterlab_pygments': 'jupyterlab-pygments',
    'libatomic_ops': 'libatomic-ops',
    'libfplll': 'fplll',
    'libgap': 'libgap-sage',
    'libgd': 'libgd2',
    'libpng': 'libpng1.6',
    'lrcalc_python': 'python-lrcalc',
    'matplotlib_inline': 'matplotlib-inline',
    'maxima': 'maxima-sage',
    'm4ri': 'libm4ri',
    'm4rie': 'libm4rie',
    'memory_allocator': 'memory-allocator',
    'meson_python': 'meson-python',
    'mpc': 'mpclib3',
    'mpfr': 'mpfr4',
    'nest_asyncio': 'python-nest-asyncio',
    'ninja_build': 'ninja-build',
    'notebook': 'jupyter-notebook',
    'packaging': 'python-packaging',
    'pari_galdata': 'pari-galdata',
    'pari_seadata_small': 'pari-seadata',
    'pathlib2': 'python-pathlib2',
    'pathpy': 'path.py',
    'pathspec': 'python-pathspec',
    'pcre': 'pcre3',
    'pip': 'python-pip',
    'pkgconfig': 'python-pkgconfig',
    'planarity': 'edge-addition-planarity-suite',
    'pluggy': 'python-pluggy',
    'poetry_core': 'poetry-core',
    'polytopes_db': 'sagemath-database-polytopes',
    'prometheus_client': 'python-prometheus-client',
    'prompt_toolkit': 'prompt-toolkit',
    'psutil': 'python-psutil',
    'pure_eval': 'python-pure-eval',
    'py': 'python-py',
    'pycrypto': 'python-crypto',
    'pyproject_metadata': 'pyproject-metadata',
    'python_openid': 'python3-openid',
    'pytz': 'python-tz',
    'pytz_deprecation_shim': 'pytz-deprecation-shim',
    'r': 'r-base',
    'sagelib': 'sagemath',
    'sagenb_export': 'sagenb-export',
    'scandir': 'python-scandir',
    'setuptools': 'python-setuptools',
    'setuptools_scm': 'setuptools-scm',
    'setuptools_scm_git_archive': 'setuptools-scm-git-archive',
    'setuptools_wheel': 'setuptools',
    'snowballstemmer': 'snowball',
    'sphinxcontrib_applehelp': 'sphinxcontrib-applehelp',
    'sphinxcontrib_devhelp': 'sphinxcontrib-devhelp',
    'sphinxcontrib_htmlhelp': 'sphinxcontrib-htmlhelp',
    'sphinxcontrib_jsmath': 'sphinxcontrib-jsmath',
    'sphinxcontrib_qthelp': 'sphinxcontrib-qthelp',
    'sphinxcontrib_serializinghtml': 'sphinxcontrib-serializinghtml',
    'sphinxcontrib_websupport': 'sphinxcontrib-websupport',
    'sphinx_basic_ng': 'sphinx-basic-ng',
    'sphinx_copybutton': 'sphinx-copybutton',
    'sqlite': 'sqlite3',
    'tinycss2': 'python-tinycss2',
    'toml': 'python-toml',
    'tomli': 'python-tomli',
    'tornado': 'python-tornado',
    'typing_extensions': 'python-typing-extensions',
    'tzlocal': 'python-tzlocal',
    'urllib3': 'python-urllib3',
    'webencodings': 'python-webencodings',
    'werkzeug': 'python-werkzeug',
    'zeromq': 'zeromq3',
    'zipp': 'python-zipp',
    'zn_poly': 'libzn-poly',
    'zope_interface': 'zope.interface',
    # optional
    'biopython': 'python-biopython',
    'boost': 'boost-defaults',
    'cbc': 'coinor-cbc',
    'csdp': 'coinor-csdp',
    'd3js': 'd3',
    'gmpy2': 'python-gmpy2',
    'gnuplotpy': 'python-gnuplot',
    'jsmath_image_fonts': 'jsmath-fonts',
    'jsonschema': 'python-jsonschema',
    'lrs': 'lrslib',
    'pandocfilters': 'python-pandocfilters',
    'python2': 'python-defaults',
    'python3': 'python3-defaults',
    'python_igraph': 'python-igraph',
    'sip': 'sip4',
    'threejs': 'three.js',
    'tomlkit': 'python-tomlkit',
    'xz': 'xz-utils',
}

UNNEEDED_PACKAGES = (
    '_prereq', # virtual package to define some dependencies
    'appnope', # only for Mac OS X
    'certifi', # tornado dep
    'gfortran', # needed but does not specify version
    'iconv', # is part of the libc
    'jsonschema', # ipython dep
    'liblzma', # part of xz-utils
    'markupsafe', # jinja2 dep
    'mpir', # we use GMP
    'pplpy_doc', # part of pplpy
    'pycygwin', # only needed for cygwin port
    'sage_conf', # part of sagemath
    'sage_docbuild', # part of sagemath
    'sage_setup', # part of sagemath
    'sagemath_doc_html', # part of sagemath
    'widgetsnbextension', # included in ipywidgets package
    # optional
    'speaklater', # used to be a flask_babel dep
    'termcap',
    # base
    'configure',
)

PIP_NAME_MAPPING = {
    'beautifulsoup': 'BeautifulSoup',
    'mercurial': 'Mercurial',
    'pyopenssl': 'pyOpenSSL',
    'sqlalchemy': 'SQLAlchemy',
    'trac': 'Trac',
}

standard_package_count = 0

def command_cache_execute(cmd):
    s = hashlib.md5(repr(cmd).encode('utf-8')).hexdigest()
    cache_file = os.path.join(CACHE_DIR, s)
    if os.path.exists(cache_file):
        with open(cache_file) as fp:
            return fp.read()
    st = st = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].decode(encoding='utf-8')
    with open(cache_file, 'w') as fp:
        fp.write(st)
    return st

def url_cache_file(url, prefix = ''):
    s = prefix + hashlib.md5(url.encode('utf-8')).hexdigest()
    return os.path.join(CACHE_DIR, s)

def url_cache_read(url, prefix = ''):
    cache_file = url_cache_file(url, prefix)
    if os.path.exists(cache_file):
        with open(cache_file, 'r') as fp:
            return fp.read()
    try:
        st = http.request('GET', url).data.decode(encoding='utf-8')
    except:
        return ''
    if not os.path.exists(CACHE_DIR):
        os.mkdir(CACHE_DIR)
    with open(cache_file, 'w') as fp:
        fp.write(st)
    return st

def soap_cache_call(p, method, **kwargs):
    s = hashlib.md5(repr((method, kwargs)).encode('utf-8')).hexdigest()
    cache_file = os.path.join(CACHE_DIR, s)
    if os.path.exists(cache_file) and (kwargs and kwargs.get('source') != 'accerciser'):
        try:
            with open(cache_file) as fp:
                return pickle.load(fp)
        except pickle.UnpicklingError:
            pass
        except EOFError:
            pass
    st = getattr(p, method)(**kwargs)
    with open(cache_file, 'w') as fp:
        pickle.dump(st, fp)
    return st

def strip_binary_rebuild_suffix(version):
    return re.sub('\+b\d+$', '', version)


class Package:
    _all_debian_package_versions = None

    name = None
    upstream_version = None

    suite = None

    def __repr__(self):
        return '<Package name=%s, upstream:%s>' % (self.name, self.upstream_version)

    def get_debian_name(self):
        return DEBIAN_NAME_MAPPING.get(self.name, self.name)
    debian_name = property(get_debian_name)

    _available_packages = None
    def get_available_packages(self):
        if self._available_packages:
            return self._available_packages

        # look for source packages
        debian_packages = None
        import copy
        for package in self._all_debian_package_versions.values():
            if not set(package.keys()).intersection(['unstable', 'experimental', 'new']):
                continue
            for suite in package.values():
                for version in suite.values():
                    if version.get('source') == self.debian_name:
                        if debian_packages:
                            for suite2 in package.keys():
                                if not suite2 in debian_packages:
                                    debian_packages[suite2] = package[suite2]
                                else:
                                    for version2 in package[suite2].keys():
                                        debian_packages[suite2][version2] = package[suite2][version2]
                        else:
                            debian_packages = copy.deepcopy(package)
                else:
                    continue
                break
            else:
                continue
            break
        if not debian_packages:
            debian_packages = self._all_debian_package_versions.get(self.debian_name)
        if not debian_packages:
            return []
            sys.exit(1)
            return self.fallback_get_available_packages()

        packages = []
        for suite in (TESTING, UNSTABLE, 'experimental', 'new'):
            for version in debian_packages.get(suite, {}):
                archs = [x for x in debian_packages[suite][version]['architectures']]
                if archs != ['source']:
                    archs = [x for x in archs if x != 'source']
                packages.append((version, suite, archs))
        packages.sort(key=cmp_to_key(lambda x,y: apt_pkg.version_compare(x[0], y[0])))
        #print self.debian_name
        #print 'packages:', packages
        return packages

    def fallback_get_available_packages(self):
        packages = []
        okayish_versions = {}
        for line in reversed(command_cache_execute(
                    ['rmadison', '-S', self.debian_name]).splitlines()):
            p, v, s, a = [x.strip() for x in line.split('|')]
            if '+b' in v:
                v = v[:v.index('+b')]
            if 'source' in a:
                okayish_versions[v] = True

        for line in reversed(command_cache_execute(
                    ['rmadison', '-S', self.debian_name]).splitlines()):
            p, v, s, a = [x.strip() for x in line.split('|')]
            if '+b' in v:
                v = v[:v.index('+b')]
            if not v in okayish_versions:
                continue
            if a == 'source':
                continue

            archs = [x.strip() for x in a.split(',') if x.strip() != 'source']
            if (v, s) in [(x[0], x[1]) for x in packages]:
                # already got a version number for that suite
                v1, s1, a1 = [x for x in packages if x[1] == s][0]

                # when a binary all package was selected, but there is another
                # binary package that is arch specific, prefer it.
                if not 'all' in a1:
                    continue
                if 'all' in a1 and 'all' in archs:
                    continue
                packages.remove((v1, s1, a1))
            packages.append((v, s, archs))
        packages.sort(key=cmp_to_key(lambda x,y: apt_pkg.version_compare(x[0], y[0])))
        self._available_packages = packages
        print(packages)
        sys.exit(1)
        return packages

    def get_debian_version(self, debian_suite):
        versions = {}
        for version, suite, archs in self.get_available_packages():
            versions[suite] = version
        return versions.get(debian_suite)

    def get_debian_pts_url(self):
        return 'https://tracker.debian.org/pkg/%s' % quote(self.debian_name)
    debian_pts_url = property(get_debian_pts_url)

    def version_quirks_debian(self, version):
        if version:
            if (self.name == 'flask_oldsessions'):
                # I (infinity0) made a mistake for the upload, the +git20121007 is redundant
                version = '0.10' if '0.10+git20121007' else version
            elif (self.name == 'gap'):
                version = re.sub('[a-z]', '.', version)
            elif (self.name == 'gmpy2'):
                version = re.sub('~', '', version)
            elif (self.name == 'lrslib'):
                version = re.sub('0\.', '0', version)
            elif (self.name == 'nauty'):
                version = re.sub('\.', '', version)
            elif (self.name == 'ncurses'):
                version = re.sub('\+', '.', version)
            elif (self.name == 'pari_galdata' or self.name == 'pari_seadata_small'):
                version = re.sub('0\.', '', version)
            elif (self.name == 'singular'):
                version = re.sub('-p', 'p', version)
        return version

    def version_quirks_sage(self, version):
        if version:
            if (self.name == 'boost'):
                version = re.sub('_', '.', version)
            elif (self.name == 'bzip2'):
                version = re.sub('-201(\d*)', '', version)
            elif (self.name == 'fpylll'):
                version = re.sub('dev', '', version)
            elif (self.name == 'iml'):
                version = re.sub('1.0.4p1', '1.0.4', version)
            elif (self.name == 'readline'):
                version = re.sub('.00\d', '', version)
            elif (self.name == 'sagemath'):
                version = re.sub('.beta', '~beta', version)
                version = re.sub('.rc', '~rc', version)
            elif (self.name == 'sqlite'):
                version = re.sub(r'(\d)(\d\d)(\d*)', lambda x: ".".join(map(str, map(int, x.groups()))), version)
            elif (self.name == 'threejs'):
                version = re.sub('r', '', version)
        return version

    def get_status_and_debian_version(self):
        suite_version = None
        debsuite_list = (TESTING, UNSTABLE, 'experimental', 'incoming', 'new', 'vcs')
        if backport:
            debsuite_list = (TESTING, UNSTABLE)
        for debsuite in debsuite_list:
            suite_version = self.get_debian_version(debsuite) or suite_version
            upstream_version_debian = self.version_quirks_debian(remove_deb_rev(self.get_debian_version(debsuite)))
            upstream_version_sage = self.version_quirks_sage(remove_sage_rev(self.upstream_version))
            debian_minus_upstream = vcmp(upstream_version_debian, upstream_version_sage)
            if debian_minus_upstream >= 0:
                if debsuite == TESTING:
                    if get_major_version(upstream_version_debian) == get_major_version(upstream_version_sage):
                        status = 'ok-testing'
                    else:
                        status = 'newer-testing'
                elif debsuite == UNSTABLE:
                    if get_major_version(upstream_version_debian) == get_major_version(upstream_version_sage):
                        status = 'ok'
                    else:
                        status = 'newer'
                elif debsuite == 'new':
                    status = 'waiting-in-new'
                elif debsuite == 'experimental':
                    status = 'ok-experimental'
                else:
                    raise ValueError("unrecognised suite: %s" % debsuite)
                if self.name in UNNEEDED_PACKAGES:
                    # package is available but not needed
                    status = 'notneeded'
                return (status, suite_version)
        else:
            if suite_version and suite_version == self.get_debian_version('new'):
                status = 'missing'
                return ('waiting-in-new', suite_version)
            elif suite_version:
                # package is available, but outdated
                if get_major_version(remove_deb_rev(suite_version)) == get_major_version(upstream_version_sage):
                    status = 'lagging-minor'
                else:
                    status = 'lagging-major'
            else:
                # package is not available
                status = 'missing'
            if self.name in UNNEEDED_PACKAGES:
                # package is not needed
                status = 'notneeded'
            return (status, suite_version)

    def print_tr(self, output, suite):
        global standard_package_count
        debian_status, suite_version = self.get_status_and_debian_version()
        if (suite == 'standard' and debian_status != 'notneeded'):
            standard_package_count += 1
        if suite_version:
            td_debian_version = '<td class="%s">%s</td>' % (debian_status, suite_version)
        else:
            td_debian_version = '<td class="%s">%s</td>' % (debian_status, debian_status)

        if debian_status in ('notneeded', 'ok-testing', 'newer-testing', 'ok', 'newer'):
            print(f'<tr class="{debian_status}">', file=output)
        else:
            print('<tr>', file=output)

        if suite == 'sagemath':
            print(f'<th><a href="https://github.com/sagemath/sage/tree/{branch}/" style="text-decoration: none">{self.name}</a></th>', file=output)
        else:
            print(f'<th><a href="https://github.com/sagemath/sage/tree/{branch}/build/pkgs/{self.name}" style="text-decoration: none">{self.name}</a></th>', file=output)

        print(f'<td>{self.upstream_version}</td>', file=output)
        print(td_debian_version, file=output)
        if suite_version:
            print(f'<td class="pts"><a href="{self.debian_pts_url}">PTS</a></td>', file=output)
        else:
            print('<td class="pts"></td>', file=output)
        print('</tr>', file=output)

def get_sage_suites():
    return ['sagemath', 'standard', 'optional', 'pip', 'base', 'experimental', 'script', 'none']

def get_sage_version(spkg_name):
    try:
        v_url = 'https://raw.githubusercontent.com/sagemath/sage/%s/build/pkgs/%s/package-version.txt' % (branch, spkg_name)
        version = url_cache_read(v_url).rstrip()
        if len(version) >= 100:
            version = 'unknown'
    except:
        version = 'none'
    return version

def get_pip_version(spkg_name):
    try:
        v_url = f'https://pypi.python.org/pypi/{spkg_name}/'
        s = url_cache_read(v_url)
        versions = re.findall(r'<a href="/pypi/%s/(\d+.*)">' % (spkg_name), s)
        version = versions[0]
        if len(version) >= 100:
            version = 'unknown'
    except:
        version = 'none'
    return version

def get_sage_suite(spkg_name):
    s_url = f'https://raw.githubusercontent.com/sagemath/sage/{branch}/build/pkgs/{spkg_name}/type'
    suite = url_cache_read(s_url).rstrip()
    if not (suite in get_sage_suites()):
        suite = 'none'
    return suite

def get_sage_packages():
    spkg_all = []
    url = f'https://api.github.com/repos/sagemath/sage/contents/build/pkgs?ref={branch}'
    lst = url_cache_read(url)
    for pkg in re.findall ("\"name\":\s?\"([^/']*)\",", lst):
        if pkg.startswith('_'):
            continue
        p=Package()
        p.name=pkg
        p.suite = get_sage_suite(pkg)
        if (p.suite == 'pip'):
            p.version = get_pip_version (PIP_NAME_MAPPING.get(pkg, pkg))
        else:
            p.version = get_sage_version (pkg)
        p.upstream_version = p.version
        spkg_all.append(p)
    '''
    p=Package()
    p.name="juffed"
    p.suite="standard"
    p.version = "0.10-85-g5ba17f9"
    p.upstream_version = "0.10-85-g5ba17f9"
    '''
    spkg_all.append(p)

    return spkg_all

def remove_deb_rev(v):
    if v is None:
        return None
    else:
        w = re.sub('\+b\d*$', '', v)
        w = re.sub('[~+]?sage\d*$', '', w)
        w = re.sub('-[\.\d]+$', '', w)
        w = re.sub('\+ds\d*$', '', w)
        w = re.sub('\+dfsg\d*$', '', w)
        return re.sub('\.p\d+$', '', w)

def remove_sage_rev(v):
    if v is None:
        return None
    else:
        return re.sub('\.p\d+$', '', v)

def vcmp(v1, v2):
    '''compare two versions, without debian artefacts'''
    if v1 is None and v2 is None:
        return 0
    if v1 is None:
        return -1
    if v2 is None:
        return 1
    if ':' in v1:
        v1 = v1[v1.index(':')+1:]
    if ':' in v2:
        v2 = v2[v2.index(':')+1:]
    return apt_pkg.version_compare(v1, v2)

def get_major_version(v):
    if ':' in v:
        v = v[v.index(':')+1:]
    return '.'.join(v.split('.')[:2])

def sagemath_package(output, version):
    p=Package()
    p.name='sagemath'
    p.suite = 'sagemath'
    p.version = version
    p.upstream_version = p.version
    return p

if __name__ == '__main__':
    if not os.path.exists(CACHE_DIR):
        os.mkdir(CACHE_DIR)

    parser = OptionParser()
    parser.add_option('--output', dest='output', metavar='OUTPUT', default='-')
    parser.add_option('--branch', dest='branch', metavar='BRANCH', default='master')
    parser.add_option('--backport', dest='backport', metavar='BACKPORT', default='-')
    options, args = parser.parse_args()

    if options.backport == '-':
        backport = False
    else:
        backport = True
        TESTING = 'stable'
        UNSTABLE = 'jessie-backports'

    branch = options.branch

    sv_url = 'https://raw.githubusercontent.com/sagemath/sage/%s/VERSION.txt' % branch
    v = url_cache_read(sv_url)
    for m in re.findall('SageMath version (.*), Release', v):
        mu = m

    if options.output == '-':
        output = sys.stdout
    else:
        output = StringIO()

    print(HTML_AT_TOP % {'version': mu, 'testing': TESTING, 'unstable': UNSTABLE}, file=output)

    modules = get_sage_packages()
    modules.append(sagemath_package(output, mu))

    Package._all_debian_package_versions = {}
    for i in range(0, len(modules), 10):
        sliced_modules = modules[i:i+10]
        url = 'https://api.ftp-master.debian.org/madison?f=json&S=true&package=%s' % quote(' '.join(
                [x.debian_name for x in sliced_modules]))
        res = json.loads(command_cache_execute(['curl', '-k', '--silent', url]))
        if res:
            Package._all_debian_package_versions.update(res[0])

    if not modules:
        print('<tr><td colspan="7">Retrieving list of packages from the Sage homepage failed this time. Come back later!</td></tr>', file=output)

    standard_modules = []
    for suite in get_sage_suites():
        print(f'<tr><td class="heading" colspan="7">{suite}</td></tr>', file=output)
        suite_modules = [x for x in modules if x.suite == suite]
        suite_modules.sort(key=lambda x: x.name.lower())
        if suite == 'standard':
            standard_modules.extend(suite_modules)
        for module in suite_modules:
            module.print_tr(output, suite)

    print('</tbody>', file=output)
    print('</table>', file=output)

    def count_debian_packages_in(*l):
      return len([x for x in standard_modules if x.get_status_and_debian_version()[0] in l])
    def udd_link():
      packagelist = ''
      for x in standard_modules:
          packagelist = packagelist + x.get_debian_name() + '+'
      return 'https://udd.debian.org/dmd/?packages=sagemath+' + packagelist

    print('<p id="summary">', file=output)
    print('%s out of %s standard packages are up-to-date in %s<br>' % (
        count_debian_packages_in('newer-testing', 'ok-testing'),
        standard_package_count,
        TESTING), file=output)
    print('%s/%s in %s, %s/%s in experimental' % (
        count_debian_packages_in('newer', 'newer-testing', 'ok', 'ok-testing'),
        standard_package_count,
        UNSTABLE,
        count_debian_packages_in('newer', 'newer-testing', 'ok', 'ok-testing', 'ok-experimental'),
        standard_package_count), file=output)
    print('</p>', file=output)

    print('</div>', file=output)

    print('<p id="footer">', file=output)
    print('Generated:', time.strftime('%Y-%m-%d %H:%M:%S %z'), file=output)
    print('on ', socket.getfqdn(), file=output)
    print('by thansen (contact: thansen at debian dot org)', file=output)
    print(', <a href="https://salsa.debian.org/science-team/sagemath-status">generation script</a>', file=output)
    print(', <a href="http://people.debian.org/~fpeters/dgs.py">original script</a>', file=output)
    if branch == 'master':
        print(', <a href="./debian-sage-dev-status.html">Sage (develop) status</a>', file=output)
    else:
        print(', <a href="./debian-sage-status.html">Sage (master) status</a>', file=output)
    print(', <a href="http://wiki.debian.org/DebianScience/Sage">Wiki</a>', file=output)
    print(', <a href="' + udd_link() + '">Dashboard</a>', file=output)
    print('</p>', file=output)

    print('</html>', file=output)

    if options.output != '-':
        with open(options.output, 'w') as fp:
            fp.write(output.getvalue())

