document.getElementsByClassName = function ( class_name ) {
    var all_obj, ret_obj = new Array(), j = 0, strict = 0;
    if ( document.getElementsByClassName.arguments.length > 1 )
        strict = ( document.getElementsByClassName.arguments[1] ? 1 : 0 );
    if ( document.all )
        all_obj = document.all;
    else if ( document.getElementsByTagName && !document.all )
        all_obj = document.getElementsByTagName ( "*" );
    for ( i = 0; i < all_obj.length; i++ ) {
        if ( ( ' ' + all_obj[i].getAttribute("class") + ' ').toLowerCase().match(
            new RegExp ( ( strict ? '^ ' + class_name + ' $' :
                '^.* ' + class_name + ' .*$' ).toLowerCase(),'g' ) ) ) {
            ret_obj[j++] = all_obj[i];
        }
    }
    return ret_obj;
}

function toggle_table_row(className, checkboxId)
{
	var t = (document.getElementById(checkboxId).checked == true);

	var tags = document.getElementsByClassName(className);
	for (var i=0; i<tags.length; i++) {
		tag = tags[i];
		if (tag.tagName.toLowerCase() != 'tr') continue;
		if (t) {
			tag.style.display = 'table-row';
		} else {
			tag.style.display = 'none';
		}
	}
}

function display_details()
{
	return toggle_table_row('arch-details', 'archs');
}

function display_unneeded()
{
	return toggle_table_row('notneeded', 'unneeded');
}

function display_ok_testing()
{
	return toggle_table_row('ok-testing', 'ok-testing');
}

function display_newer_testing()
{
	return toggle_table_row('newer-testing', 'newer-testing');
}

function display_ok_unstable()
{
	return toggle_table_row('ok', 'ok-unstable');
}

function display_newer()
{
	return toggle_table_row('newer', 'newer');
}
