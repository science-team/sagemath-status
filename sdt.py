#! /usr/bin/env python3

# sdt.py -- generation script for the Sage doctests in Debian status page
# Copyright (C) 2017      Tobias Hansen
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import re
import ssl
from urllib.request import urlopen
from urllib.parse import unquote
import time
import socket
from optparse import OptionParser

HTML_AT_TOP = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Status of Sage doctests in Debian %s</title>
<style type="text/css">
html, body {
    font-family: sans-serif;
    margin: 0;
    padding: 0;
    background: #eee;
}
#logo {
    position: absolute;
    top: 10px;
    left: 10px;
    border: 0px;
    z-index: 10;
}
#logo2 {
    position: absolute;
    top: 10px;
    left: 84px;
    border: 0px;
    z-index: 10;
}

#header h1 {
    margin: 0;
    padding: 5px 0px 5px 158px;
    background: #555753;
    color: white;
    margin-bottom: 1em;
    border-bottom: 2px solid #aaa;
}

div#content {
    margin: 2em;
    margin-right: 15em;
}

tfoot th, thead th {
    font-weight: normal;
}
tr.notneeded {
    display: none;
}

td.heading {
    text-align: center;
    background: #555753;
    color: white;
    font-weight: bold;
}
td.heading2 {
    text-align: right;
    background: none;
    font-weight: bold;
}
td.test {
    text-align: left;
}
td.arch {
    text-align: center;
    font-weight: bold;
    background: none;
}

td.Installed {background-color: #7fffd4;}
td.Uploaded {background-color: #7fffd4;}
td.Built {background-color: #7fffd4;}
td.Attempted {background-color: #ffc0cb;}

tbody th, tbody td {
    background: #d3d7cf;
}
tbody th, tbody td.pts {
    text-align: left;
}
tbody td {
    text-align: center;
}
tfoot td {
    padding-top: 1em;
    vertical-align: top;
}
td.pts a {
    font-size: small;
    padding: 0 1ex;
}
p#footer {
    font-size: small;
    color: gray;
    margin: 0;
}
div#control {
    text-align: right;
    position: fixed;
    right: 2em;
    font-size: small;
    width: 15em;
}
</style>
</head>
<body>
 <div id="header">
  <img id="logo" src="sage64.png" alt="SAGE">
  <img id="logo2" src="debian64.png" alt="Debian">
  <h1>Status of Sage doctests in Debian %s</h1>
 </div>
 <div id="content">

<table>'''

archs = []
arch_index = {}
arch_link = {}
arch_version = {}
arch_status = {}

results = []
tests = []
tablespan = 1

SslContext = ssl.create_default_context()
SslContext.load_verify_locations(cafile='cacert.pem')

def parse_buildd_d_o():
    global archs
    data = urlopen(url, context=SslContext).read()
    overview_site = data.decode('utf-8')
    index = 0
    for (arch,ver,stamp,status) in re.findall('<a href="fetch.php\?pkg=sagemath&amp;arch=(.*?)&amp;ver=(.*?)&amp;stamp=(\d*)&amp;raw=\d*">(.*?)</a></td>', overview_site):
        arch_index[arch] = index
        arch_link[arch] = 'https://buildd.debian.org/status/fetch.php?pkg=sagemath&arch=%s&ver=%s&stamp=%s&raw=1' % (arch, ver, stamp)
        arch_version[arch] = unquote(ver)
        arch_status[arch] = status
        index = index + 1
    archs = sorted(list(arch_index.keys()))
    return

def parse_logs():
    global results
    global tests
    global tablespan
    results = [None] * len(arch_index)
    testset = set()
    for arch in archs:
        results[arch_index[arch]] = []
        data = urlopen(arch_link[arch], context=SslContext).read()
        log = data.decode('utf-8')
        index = 0
        test_results_list = re.findall ("\n\-{70}\n(.*?)\n\-{70}\nTotal time for all tests: [\d\.]+ seconds\n    cpu time: [\d\.]+ seconds\n    cumulative wall time: [\d\.]+ seconds", log, flags=re.DOTALL)
        for test_results in test_results_list:
            tr = re.sub('.*\n\-{70}\n', '', test_results)
            results[arch_index[arch]].append({})
            total = 0
            for pl in re.findall("sage.* (.*)  \# (\d*) doctest", tr):
                results[arch_index[arch]][index][pl[0]] = pl[1]
                testset.add(pl[0])
                total = total + int(pl[1])
            for pl in re.findall("sage.* (.*)  \# Timed out", tr):
                results[arch_index[arch]][index][pl] = 'T'
                testset.add(pl)
            results[arch_index[arch]][index]['total'] = total
            testset.add('total')
            index = index + 1
            tablespan = tablespan + 1
        if len(test_results_list) == 0:
            results[arch_index[arch]].append({})
            tablespan = tablespan + 1
    tests = sorted(list(testset))
    return

def print_tr(test, out):
    print('<tr><td class="test">%s</td>' % test, file=out)
    for arch in archs:
        for res in results[arch_index[arch]]:
            if test in res:
                print('<td>%s</td>' % res[test], file=out)
            else:
                print('<td></td>', file=out)
    print('</tr>', file=out)

if __name__ == '__main__':
    global url

    parser = OptionParser()
    parser.add_option('--output', dest='output', metavar='OUTPUT', default='sage-test-status.html')
    parser.add_option('--suite', dest='suite', metavar='SUITE', default='unstable')
    options, args = parser.parse_args()
    out=open(options.output, "w")
    suite=options.suite
    url='https://buildd.debian.org/status/package.php?p=sagemath&suite=%s' % suite

    parse_buildd_d_o()
    parse_logs()

    print(HTML_AT_TOP % (suite, suite), file=out)

    print('<tr><td class="heading2"><a href="%s">Logs:</a></td>' % url, file=out)
    for arch in archs:
        print('<td class="arch" colspan="%d"><a href="%s">%s</a></td>' % (len(results[arch_index[arch]]), arch_link[arch], arch), file=out)
    print('</tr><tr><td class="heading2">Version:</td>', file=out)
    for arch in archs:
        print('<td colspan="%d">%s</td>' % (len(results[arch_index[arch]]), arch_version[arch]), file=out)
    print('</tr><tr><td class="heading2">Status:</td>', file=out)
    for arch in archs:
        print('<td class="%s" colspan="%d">%s</td>' % (re.sub('Build-', '', arch_status[arch]), 
                       len(results[arch_index[arch]]), re.sub('Build-', '', arch_status[arch])), file=out)
    print('</tr><tr><td class="heading2">Parallel or Serial:</td>', file=out)
    for arch in archs:
        print('<td>p</td>', file=out)
        if len(results[arch_index[arch]]) == 2:
            print('<td>s</td>', file=out)
    print('</tr><tr><td class="heading" colspan="%d">Number of failed doctests by file</td></tr>' % tablespan, file=out)
    for test in tests:
        print_tr(test, out)

    print ('</table></body>', file=out)
    print ('<p id="footer">', file=out)
    print ('Generated:', time.strftime('%Y-%m-%d %H:%M:%S %z'), file=out)
    print ('on ', socket.getfqdn(), file=out)
    print ('by thansen (contact: thansen at debian dot org)', file=out)
    print (', <a href="https://salsa.debian.org/science-team/sagemath-status">generation script</a>', file=out)
    print (', <a href="http://wiki.debian.org/DebianScience/Sage">Wiki</a>', file=out)
    print ('</p></html>', file=out)
