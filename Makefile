PYTHON = python

all: debian-sage-status.html debian-sage-dev-status.html

clean-data:
	rm -rf debian-sage-status-cache

external-data:

debian-sage-status.html: dss.py external-data Makefile
	$(PYTHON) $< --output $@

debian-sage-dev-status.html: dss.py external-data Makefile
	$(PYTHON) $< --output $@ --branch develop

.PHONY: all external-data clean-data
